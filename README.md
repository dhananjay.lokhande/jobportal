# JobPortal

## Getting started

1. clone project on local
2. go to <dir>/jobportal/
3. npm i
4. npm start
5. load http://localhost:3000 in chrome



Features:

Expandable jobs list
On click of Apply, opens new page as per defined url.
On click on View ad, it opens new page.
Typescript
Simple react app with props. No need of global storage as it is simple app.


Dir:

Containers: Holds application pages
Components: Generic and reusable components
Common: Common files like constants and types
Service: Service files for CRUD ops
