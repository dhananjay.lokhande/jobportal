import React from 'react';
import JobsList from './containers/jobs-list/JobsListPage';

function App() {
  return (
    <div className="App">
      <JobsList></JobsList>
    </div>
  );
}

export default App;
