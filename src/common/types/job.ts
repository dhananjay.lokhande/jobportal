
export type Job = {
    id: string,
    benefits: string[],
    categories: string[],
    descr: string,
    company: CompanyInfo,
    contact: contactPerson,
    departments: string[],
    employment_type: string,
    experience: string,
    function: string,
    language: string,
    layers_1: string[],
    layers_2: string[],
    layers_3: string[],
    layers_4: string[],
    layers_5: string[],
    linkedInCompanyId: number,
    locations: LocationInfo[],
    slug: string,
    title: string,
    to_date?: string,
    urls: Urls,
    video: VideoInfo,
    internal_reference?: string,
    owner: JobPostOwner,
    skills: string
};

export type JobPostOwner = {
    id: string,
    name: string,
    email: string
};
export type  Urls = {
    ad: string,
    apply: string
};

export type VideoInfo = {
    content?: string,
    url: string
}

export type LocationInfo = {
    location: LocationName
}

export type LocationName = {
    text: string
}

export type CompanyInfo = {
    id: string,
    slug: string,
    name: string,
    name_internal: string,
    website: string,
    industry: string,
    descr: string,
    logo: string,
    cover: string
};

export type contactPerson = {
    name: string,
    email: string,
    phone: string,
    photo: string
};

export type BaseServiceResponse = {
    data?: Job[] | undefined,
    statusCode?: number,
    errorMessages: string[] 
};


