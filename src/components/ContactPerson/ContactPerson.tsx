import { Typography } from "@mui/material";
import { contactPerson } from "../../common/types/job"

type ContactPersonProps = {
    contactPerson: contactPerson
};

export default function ContactPerson (props: ContactPersonProps) {
 return (
    <Typography variant="body2" color="text.secondary">
                <div className="header-text">Contact person:</div>
                {
                  props.contactPerson.photo && <img src={props.contactPerson.photo} alt={props.contactPerson.name} width="300" height="300"></img>
                }
                <div>{props.contactPerson.name}</div>
                {
                  props.contactPerson.email && <div>{props.contactPerson.email}</div>
                }
                {
                  props.contactPerson.phone && <div>{props.contactPerson.phone}</div>
                }
              </Typography>
 );
}