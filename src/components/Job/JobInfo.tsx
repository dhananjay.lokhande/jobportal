import { useState } from "react";
import { Job, LocationInfo } from '../../common/types/job';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import IconButton, { IconButtonProps } from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

import './JobInfo.scss';
import { Button } from "@mui/material";
import ContactPerson from "../ContactPerson/ContactPerson";

type JobProps = {
  job: Job
};

interface ExpandMoreProps extends IconButtonProps {
  expand: boolean;
}

const ExpandMore = styled((props: ExpandMoreProps) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

export default function JobInfo(props: JobProps) {
  const [expanded, setExpanded] = useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const getJobLocations = (locations: LocationInfo[]) => {
    return locations.map(locationInfo => locationInfo.location.text).join(',');
  }

  const getBenefits = (benefits: string[]) => {
    return benefits.map((benefit, index) => {
      return <div key={index}>{benefit}</div>;
    });
  }

  const onApplyClick = (url: string) => {
    window.open(url, '_blank', 'noopener,noreferrer');
  }

  return (
    <div className="job-info-wrapper">
      <Card sx={{ maxWidth: '100%' }}>
        <CardHeader
          action={
            <Button variant="contained" onClick={() => onApplyClick(props.job.urls.apply)}>Apply</Button>
          }
          title={props.job.title}
          subheader={props.job.company.name}
        />

        <CardContent>

          <Typography variant="body2" color="text.secondary">
            <span>Job type: {props.job.employment_type}</span>
          </Typography>

          <Typography variant="body2" color="text.secondary">
            <span>Job location: {getJobLocations(props.job.locations)}</span>
          </Typography>

          <Typography variant="body2" color="text.secondary">
            <span>Experience: {props.job.experience}</span>
          </Typography>

          <Typography variant="body2" color="text.secondary">
            <span>Language needed: {props.job.language}</span>
          </Typography>

          <Typography variant="body2" color="text.secondary">
          <span>More info about ad: <a href={props.job.urls.ad} target="_blank" rel="noreferrer">View</a></span>
          </Typography>

          {
            props.job.to_date && <Typography variant="body2" color="text.secondary">
              <span>Job application will be accepted till date {props.job.to_date}</span>
            </Typography>
          }

          <Typography variant="body2" color="text.secondary">
            <span className="header-text">Job description:</span>
            <div dangerouslySetInnerHTML={{ __html: props.job.descr }} />
          </Typography>
        </CardContent>
        <CardActions disableSpacing>
          <ExpandMore
            expand={expanded}
            onClick={handleExpandClick}
            aria-expanded={expanded}
            aria-label="show more"
          >
            <ExpandMoreIcon />
          </ExpandMore>
        </CardActions>
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <CardContent>
            <Typography variant="body2" color="text.secondary">
              <span className="header-text">Skills required:</span>
              <div dangerouslySetInnerHTML={{ __html: props.job.skills }} />
            </Typography>

            {
              props.job.benefits.length > 0 && <Typography variant="body2" color="text.secondary">
                <span className="header-text">Benefits:</span>
                <div>{getBenefits(props.job.benefits)}</div>
              </Typography>
            }

            {
              props.job.contact.name && <ContactPerson contactPerson={props.job.contact}/>
            }


          </CardContent>
        </Collapse>
      </Card>
    </div>

  )
}