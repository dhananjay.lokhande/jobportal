import { useEffect, useState } from "react";
import { LOAD_JOBS_URL } from "../../common/constants/constants";
import { BaseServiceResponse, Job } from "../../common/types/job";
import JobInfo from "../../components/Job/JobInfo";
import { loadJobsSvc } from "../../services/JobsSvc";

import './JobsListPage.scss';

export default function JobsList() {

  const [jobs, setJobs] = useState<Job[]>([]);
  const [errors, setErrors] = useState<string[]>([]);
  
  useEffect(() => {
    if (jobs.length === 0) {
      loadJobs();
    }
  }, [jobs]);

  const getServiceErrors = () => {
    return errors.map((error, index) => {
      return <div key={index}>{error}</div>
    });
  }

  const loadJobs = () => {
    loadJobsSvc(LOAD_JOBS_URL).then((response: BaseServiceResponse) => {
      setJobs(response.data as Job[]);
      setErrors([]);
    }).catch((error: BaseServiceResponse) => {
      setJobs([]);
      setErrors(error.errorMessages)
    });
  };

  const displayJobs = () => {
    return jobs.map(job => {
      return <JobInfo key={job.id} job={job}></JobInfo>;
    });
  }

  return (
    <div className='job-list-wrapper'>
      <div className="error-info">
        {
          errors.length > 0 &&
          getServiceErrors()
        }

      </div>
      <div className="job-list">
        {
          displayJobs()
        }
      </div>
    </div>
  );
}