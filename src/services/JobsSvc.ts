import axios, {AxiosError, AxiosResponse} from "axios"
import { BaseServiceResponse } from "../common/types/job";

const axiosInstance = axios.create({
    proxy: false
});

/* type Header = {
    'Content-type'?: string,
    'Authorization'?: string,
    'cache-control'?: string
}; */

export async function loadJobsSvc (url: string) {
    return axiosInstance.get(url, undefined)
    .then((response: AxiosResponse) => {
        return createSuccessResponse(response);
    }).catch((error: AxiosError) => {
       return createErrorResponse(error);
    });
}

const createSuccessResponse = (response: AxiosResponse): BaseServiceResponse => {
    const processedResponse: BaseServiceResponse = {
        data: response.data,
        statusCode: response.status,
        errorMessages: []
    };

    return processedResponse;
};

const createErrorResponse = (error: AxiosError): BaseServiceResponse => {
    const processedResponse: BaseServiceResponse = {
        data: undefined,
        statusCode: error.response?.status,
        errorMessages: [error.message]
    };

    return processedResponse;
}; 